# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
#
# Victor Ibragimov <victor.ibragimov@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-02-21 00:39+0000\n"
"PO-Revision-Date: 2019-10-10 07:59+0500\n"
"Last-Translator: Victor Ibragimov <victor.ibragimov@gmail.com>\n"
"Language-Team: English <kde-i18n-doc@kde.org>\n"
"Language: tg\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.3\n"

#: kcm.cpp:73
#, fuzzy, kde-format
#| msgid "Do not disturb:"
msgid "Toggle do not disturb"
msgstr "Ба ман халал нарасонед:"

#: sourcesmodel.cpp:393
#, fuzzy, kde-format
#| msgid "Applications"
msgid "Other Applications"
msgstr "Барномаҳо"

#: ui/ApplicationConfiguration.qml:91
#, kde-format
msgid "Show popups"
msgstr "Намоиш додани равзанаҳои пайдошаванда"

#: ui/ApplicationConfiguration.qml:105
#, kde-format
msgid "Show in do not disturb mode"
msgstr "Намоиш додан дар реҷаи \"Ба ман халал нарасонед\""

#: ui/ApplicationConfiguration.qml:118 ui/main.qml:163
#, kde-format
msgid "Show in history"
msgstr "Намоиш додан дар таърих"

#: ui/ApplicationConfiguration.qml:129
#, kde-format
msgid "Show notification badges"
msgstr "Намоиш додани нишонаҳои огоҳӣ"

#: ui/ApplicationConfiguration.qml:164
#, fuzzy, kde-format
#| msgid "Configure Events..."
msgctxt "@title:table Configure individual notification events in an app"
msgid "Configure Events"
msgstr "Танзимоти рӯйдодҳо..."

#: ui/ApplicationConfiguration.qml:172
#, kde-format
msgid ""
"This application does not support configuring notifications on a per-event "
"basis"
msgstr ""

#: ui/ApplicationConfiguration.qml:263
#, kde-format
msgid "Show a message in a pop-up"
msgstr ""

#: ui/ApplicationConfiguration.qml:272
#, kde-format
msgid "Play a sound"
msgstr ""

#: ui/main.qml:37
#, fuzzy, kde-format
#| msgid "Application Settings"
msgctxt "@action:button Application-specific notifications"
msgid "Configure Application Settings…"
msgstr "Танзимоти барнома"

#: ui/main.qml:57
#, kde-format
msgid ""
"Could not find a 'Notifications' widget, which is required for displaying "
"notifications. Make sure that it is enabled either in your System Tray or as "
"a standalone widget."
msgstr ""

#: ui/main.qml:68
#, kde-format
msgctxt "Vendor and product name"
msgid "Notifications are currently provided by '%1 %2' instead of Plasma."
msgstr ""

#: ui/main.qml:72
#, kde-format
msgid "Notifications are currently not provided by Plasma."
msgstr ""

#: ui/main.qml:79
#, fuzzy, kde-format
#| msgid "Do not disturb:"
msgctxt "@title:group"
msgid "Do Not Disturb mode"
msgstr "Ба ман халал нарасонед:"

#: ui/main.qml:84
#, kde-format
msgctxt "Automatically enable Do Not Disturb mode when screens are mirrored"
msgid "Enable automatically:"
msgstr ""

#: ui/main.qml:85
#, kde-format
msgctxt "Automatically enable Do Not Disturb mode when screens are mirrored"
msgid "When screens are mirrored"
msgstr ""

#: ui/main.qml:97
#, kde-format
msgctxt "Automatically enable Do Not Disturb mode during screen sharing"
msgid "During screen sharing"
msgstr ""

#: ui/main.qml:112
#, kde-format
msgctxt "Keyboard shortcut to turn Do Not Disturb mode on and off"
msgid "Manually toggle with shortcut:"
msgstr ""

#: ui/main.qml:119
#, kde-format
msgctxt "@title:group"
msgid "Visibility conditions"
msgstr ""

#: ui/main.qml:124
#, kde-format
msgid "Critical notifications:"
msgstr "Огоҳиҳои танқидӣ:"

#: ui/main.qml:125
#, fuzzy, kde-format
#| msgid "Show in do not disturb mode"
msgid "Show in Do Not Disturb mode"
msgstr "Намоиш додан дар реҷаи \"Ба ман халал нарасонед\""

#: ui/main.qml:137
#, fuzzy, kde-format
#| msgid "Critical notifications:"
msgid "Normal notifications:"
msgstr "Огоҳиҳои танқидӣ:"

#: ui/main.qml:138
#, kde-format
msgid "Show over full screen windows"
msgstr ""

#: ui/main.qml:150
#, kde-format
msgid "Low priority notifications:"
msgstr ""

#: ui/main.qml:151
#, kde-format
msgid "Show popup"
msgstr "Намоиш додани равзанаи пайдошаванда"

#: ui/main.qml:180
#, fuzzy, kde-format
#| msgid "Popup Position"
msgctxt "@title:group As in: 'notification popups'"
msgid "Popups"
msgstr "Мавқеъи пайдошавии равзана"

#: ui/main.qml:186
#, fuzzy, kde-format
#| msgid "Applications:"
msgctxt "@label"
msgid "Location:"
msgstr "Барномаҳо:"

#: ui/main.qml:187
#, fuzzy, kde-format
#| msgctxt "Show application jobs in notification widget"
#| msgid "Show in notifications"
msgctxt "Popup position near notification plasmoid"
msgid "Near notification icon"
msgstr "Намоиш додан дар огоҳиҳо"

#: ui/main.qml:224
#, fuzzy, kde-format
#| msgid "Choose Custom Position..."
msgid "Choose Custom Position…"
msgstr "Интихоби мавқеъи фармоишӣ..."

#: ui/main.qml:233 ui/main.qml:249
#, kde-format
msgid "%1 second"
msgid_plural "%1 seconds"
msgstr[0] "%1 сония"
msgstr[1] "%1 сония"

#: ui/main.qml:238
#, fuzzy, kde-format
#| msgid "Hide popup after:"
msgctxt "Part of a sentence like, 'Hide popup after n seconds'"
msgid "Hide after:"
msgstr "Пинҳон кардани равзанаи пайдошаванда баъд аз:"

#: ui/main.qml:261
#, kde-format
msgctxt "@title:group"
msgid "Additional feedback"
msgstr ""

#: ui/main.qml:267
#, kde-format
msgctxt "Show application jobs in notification widget"
msgid "Show in notifications"
msgstr "Намоиш додан дар огоҳиҳо"

#: ui/main.qml:268
#, kde-format
msgid "Application progress:"
msgstr "Раванди барнома:"

#: ui/main.qml:282
#, kde-format
msgctxt "Keep application job popup open for entire duration of job"
msgid "Keep popup open during progress"
msgstr ""

#: ui/main.qml:295
#, kde-format
msgid "Notification badges:"
msgstr ""

#: ui/main.qml:296
#, kde-format
msgid "Show in task manager"
msgstr "Намоиш додан дар мудири вазифаҳо"

#: ui/PopupPositionPage.qml:14
#, kde-format
msgid "Popup Position"
msgstr "Мавқеъи пайдошавии равзана"

#: ui/SourcesPage.qml:19
#, kde-format
msgid "Application Settings"
msgstr "Танзимоти барнома"

#: ui/SourcesPage.qml:99
#, kde-format
msgid "Applications"
msgstr "Барномаҳо"

#: ui/SourcesPage.qml:100
#, kde-format
msgid "System Services"
msgstr "Хизматҳои низомӣ"

#: ui/SourcesPage.qml:148
#, kde-format
msgid "No application or event matches your search term"
msgstr ""

#: ui/SourcesPage.qml:171
#, kde-format
msgid ""
"Select an application from the list to configure its notification settings "
"and behavior"
msgstr ""

#, fuzzy
#~| msgid "Application Settings"
#~ msgctxt "@title:group"
#~ msgid "Application-specific settings"
#~ msgstr "Танзимоти барнома"

#, fuzzy
#~| msgid "Configure..."
#~ msgid "Configure…"
#~ msgstr "Танзимот..."

#~ msgid "Configure Notifications"
#~ msgstr "Танзимоти огоҳиҳо"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Victor Ibragimov"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "victor.ibragimov@gmail.com"

#~ msgid "Notifications"
#~ msgstr "Огоҳиҳо"

#~ msgid "Always keep on top"
#~ msgstr "Ҳамеша дар боло нигоҳ дошта шавад"

#~ msgid "Popup position:"
#~ msgstr "Мавқеъи пайдошавии равзана:"
