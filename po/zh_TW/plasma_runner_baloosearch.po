# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Franklin, 2014.
# Jeff Huang <s8321414@gmail.com>, 2016.
# pan93412 <pan93412@gmail.com>, 2018.
# Kisaragi Hiu <mail@kisaragi-hiu.com>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-07-10 02:12+0000\n"
"PO-Revision-Date: 2023-03-06 21:51+0900\n"
"Last-Translator: Kisaragi Hiu <mail@kisaragi-hiu.com>\n"
"Language-Team: Traditional Chinese <zh-l10n@linux.org.tw>\n"
"Language: zh_TW\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Lokalize 22.12.2\n"

#: baloosearchrunner.cpp:60
#, kde-format
msgid "Open Containing Folder"
msgstr "開啟所在資料夾"

#: baloosearchrunner.cpp:82
#, kde-format
msgid "Audios"
msgstr "音效"

#: baloosearchrunner.cpp:83
#, kde-format
msgid "Images"
msgstr "影像"

#: baloosearchrunner.cpp:84
#, kde-format
msgid "Videos"
msgstr "影片"

#: baloosearchrunner.cpp:85
#, kde-format
msgid "Spreadsheets"
msgstr "試算表"

#: baloosearchrunner.cpp:86
#, kde-format
msgid "Presentations"
msgstr "簡報"

#: baloosearchrunner.cpp:87
#, kde-format
msgid "Folders"
msgstr "資料夾"

#: baloosearchrunner.cpp:88
#, kde-format
msgid "Documents"
msgstr "文件"

#: baloosearchrunner.cpp:89
#, kde-format
msgid "Archives"
msgstr "封存"

#: baloosearchrunner.cpp:90
#, kde-format
msgid "Texts"
msgstr "文字"

#: baloosearchrunner.cpp:91
#, kde-format
msgid "Files"
msgstr "檔案"
